<?php

function fueltech_theme_scripts()
{

    wp_deregister_script('jquery');

    wp_register_script('js-script', get_template_directory_uri() . '/js/jquery.js', array(), '1.11.0', true);

    $translation_array = array('template_directory_uri' => get_template_directory_uri());

    wp_localize_script('js-script', 'my_data', $translation_array);

    wp_enqueue_script('js-script');

    wp_register_script('owl2', get_template_directory_uri() . '/js/owlcarousel/owl.carousel.min.js', array(), '2.0.0', true);

    wp_enqueue_script('owl2');

    wp_register_script('videojs', 'http://vjs.zencdn.net/5.6.0/video.js?ver=1.11.0', array(), '2.0.0', true);

    wp_enqueue_script('videojs');

    wp_register_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js', array(), '2.0.0', true);

    wp_enqueue_script('modernizr');

    wp_register_script( 'matchmedia', get_template_directory_uri().'/js/matchmedia.js', array(), '1.11.0', true );

    wp_enqueue_script( 'matchmedia' );

    wp_register_script('animate-scroll', get_template_directory_uri() . '/js/animatescroll.js', array(), '2.0.0', true);

    wp_enqueue_script('animate-scroll');

    wp_register_script('scrollbar', get_template_directory_uri() . '/js/jquery.nicescroll.min.js', array(), '1.11.0', true);

    wp_enqueue_script('scrollbar');

    wp_register_script('waypoint', get_template_directory_uri() . '/js/waypoint.js', array(), '1.11.0', true);

    wp_enqueue_script('waypoint');

    wp_register_script('touch-events', get_template_directory_uri() . '/js/touch-events.js', array(), '1.11.0', true);

    wp_enqueue_script('touch-events');


    wp_register_script('flip', get_template_directory_uri() . '/js/jquery.flip.min.js', array(), '1.11.0', true);

    wp_enqueue_script('flip');

    wp_register_script('velocity', get_template_directory_uri() . '/js/velocity.js', array(), '1.11.0', true);

    wp_enqueue_script('velocity');

    wp_register_script('velocity-ui', get_template_directory_uri() . '/js/velocity-ui.js', array(), '1.11.0', true);

    wp_enqueue_script('velocity-ui');

    wp_register_script('function-toggle', get_template_directory_uri() . '/js/function-toggle.js', array(), '1.11.0', true);

    wp_enqueue_script('function-toggle');

    wp_register_script('fueltech-js', get_template_directory_uri() . '/js/fueltech.js', array(), '1.11.0', true);

    wp_localize_script('fueltech-js', 'ajax_object', array('ajaxurl' => admin_url('admin-ajax.php')));

    wp_enqueue_script('fueltech-js');

    wp_enqueue_style('styles', get_stylesheet_uri());

}

add_action('wp_enqueue_scripts', 'fueltech_theme_scripts');

// enable svg support in wordpress
function custom_mtypes($m)
{
    $m['svg'] = 'image/svg+xml';
    $m['svgz'] = 'image/svg+xml';
    return $m;
}

add_filter('upload_mimes', 'custom_mtypes');

add_action('init', 'publicize_custom_post_type');
function publicize_custom_post_type()
{
    add_post_type_support('news', 'publicize');
}

// ------------------------------------------------------------------
// Featured Image Support
// ------------------------------------------------------------------
//

add_theme_support('post-thumbnails');

// ------------------------------------------------------------------
// Theme Customizer
// ------------------------------------------------------------------
//


include 'theme_customizer.php';


// ------------------------------------------------------------------
// Menu
// ------------------------------------------------------------------
//

function menus()
{

    register_nav_menus(array(

        'primary-menu' => __('Primary Menu'),

    ));

}

add_action('init', 'menus');

$nav_logo = get_theme_mod('company_logo');

$home_url = get_home_url();

// ------------------------------------------------------------------
// Menu Walker
// ------------------------------------------------------------------
//

include_once('menu_walker.php');

// ------------------------------------------------------------------
// Enable full Tiny MCE eDITOR
// ------------------------------------------------------------------
//

function enable_more_buttons($buttons)
{

    $buttons[] = 'fontselect';
    $buttons[] = 'fontsizeselect';
    $buttons[] = 'styleselect';
    $buttons[] = 'backcolor';
    $buttons[] = 'newdocument';
    $buttons[] = 'cut';
    $buttons[] = 'copy';
    $buttons[] = 'charmap';
    $buttons[] = 'hr';
    $buttons[] = 'blockquote';

    return $buttons;
}

add_filter('mce_buttons_3', 'enable_more_buttons');

function myformatTinyMCE($in)
{

    $in['wordpress_adv_hidden'] = FALSE;

    return $in;
}

add_filter('tiny_mce_before_init', 'myformatTinyMCE');

// ------------------------------------------------------------------
// Allow span tags in tiny MCE
// ------------------------------------------------------------------
//

function myextensionTinyMCE($init)
{

    // Command separated string of extended elements
    $ext = 'span[id|name|class|style]';

    // Add to extended_valid_elements if it alreay exists
    if (isset($init['extended_valid_elements'])) {
        $init['extended_valid_elements'] .= ',' . $ext;
    } else {
        $init['extended_valid_elements'] = $ext;
    }

    // Super important: return $init!
    return $init;
}

add_filter('tiny_mce_before_init', 'myextensionTinyMCE');


// Remove 'view' link and permalink from specified custom post types

add_filter('get_sample_permalink_html', 'my_hide_permalinks', 10, 5);

function my_hide_permalinks($return, $post_id, $new_title, $new_slug, $post)
{
    if ($post->post_type === 'home_page' || $post->post_type === 'banner' || $post->post_type === 'portfolio') {
        return '';
    }
    return $return;
}

add_filter('post_row_actions', 'remove_row_actions', 10, 1);

function remove_row_actions($actions)
{
    if (get_post_type() === 'home_page' || get_post_type() === 'banner' || $post->post_type === 'portfolio')
        unset($actions['view']);
    return $actions;
}

// ------------------------------------------------------------------
// Portfolio AJAX Calls
// ------------------------------------------------------------------
//

include('portfolio-ajax.php');

include('portfolio-project-ajax.php');

// ------------------------------------------------------------------
// We Innovate AJAX Calls
// ------------------------------------------------------------------
//

include('we-innovate-ajax.php');

// ------------------------------------------------------------------
// Breadcrumbs
// ------------------------------------------------------------------
//

function custom_breadcrumbs() {

    // Settings
    $separator          = '<span class="pipe">|</span>';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = 'We Empower';

    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';

    // Get the query & post information
    global $post,$wp_query;

    // Do not display on the homepage
    if ( !is_front_page() ) {

        // Build the breadcrums
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';

        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li class="separator separator-home"> ' . $separator . ' </li>';

        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {

            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';

        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {

            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if($post_type != 'post') {

                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';

            }

            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';

        } else if ( is_single() ) {

            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if($post_type != 'post') {

                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';

            }

            // Get post category info
            $category = get_the_category();

            if(!empty($category)) {

                // Get last category post is in
                $last_category = end(array_values($category));

                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);

                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }

            }

            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {

                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;

            }

            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

                // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {

                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

            } else {

                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

            }

        } else if ( is_category() ) {

            // Category page
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';

        } else if ( is_page() ) {

            // Standard page
            if( $post->post_parent ){

                // If child page, get parents
                $anc = get_post_ancestors( $post->ID );

                // Get parents in the right order
                $anc = array_reverse($anc);

                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }

                // Display parent pages
                echo $parents;

                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';

            } else {

                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';

            }

        } else if ( is_tag() ) {

            // Tag page

            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;

            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';

        } elseif ( is_day() ) {

            // Day archive

            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';

            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';

            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';

        } else if ( is_month() ) {

            // Month Archive

            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';

            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';

        } else if ( is_year() ) {

            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';

        } else if ( is_author() ) {

            // Auhor archive

            // Get the author information
            global $author;
            $userdata = get_userdata( $author );

            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';

        } else if ( get_query_var('paged') ) {

            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';

        } else if ( is_search() ) {

            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';

        } elseif ( is_404() ) {

            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }

        echo '</ul>';

    }

}


// ------------------------------------------------------------------
// Page Slug Body Class
// ------------------------------------------------------------------
//
function add_slug_body_class( $classes ) {
    global $post;
    if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


// ------------------------------------------------------------------
// Numeric Pagination for WP Query
// ------------------------------------------------------------------
//

function custom_pagination($numpages = '', $pagerange = '', $paged = '')
{

    if (empty($pagerange)) {

        $pagerange = 2;

    }

    /**
     * This first part of our function is a fallback
     * for custom pagination inside a regular loop that
     * uses the global $paged and global $wp_query variables.
     *
     * It's good because we can now override default pagination
     * in our theme, and use this function in default quries
     * and custom queries.
     */

    global $paged;

    if (empty($paged)) {

        $paged = 1;
    }

    if ($numpages == '') {

        global $wp_query;

        $numpages = $wp_query->max_num_pages;

        if (!$numpages) {

            $numpages = 1;

        }

    }

    /**
     * We construct the pagination arguments to enter into our paginate_links
     * function.
     */

    $pagination_args = array(
        'base'            => get_pagenum_link(1) . '%_%',
        'format'          => 'page/%#%',
        'total'           => $numpages,
        'current'         => $paged,
        'show_all'        => False,
        'end_size'        => 1,
        'mid_size'        => $pagerange,
        'prev_next'       => false,
        'prev_text'       => __('&laquo;'),
        'next_text'       => __('&raquo;'),
        'type'            => 'plain',
        'add_args'        => false,
        'add_fragment'    => '',
        'before_page_number' => '<span class="item">',
        'after_page_number' => '</span>'

    );

    $paginate_links = paginate_links($pagination_args);

    if ($paginate_links) {

        echo "<nav class='ui pagination menu'>";

        echo $paginate_links;

        echo "</nav>";

    }

}